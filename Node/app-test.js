"use strict";

require("./helpers/setup");

var wd = require('wd');
var _ = require('underscore');
var assert = require("chai").assert;

describe('Appium App Test', function() {
  this.timeout(30000);
  var driver; //configure the terminal as a driver
  
  before(function() {
      // starts the browser
      var serverConfig = require('./helpers/appium-servers').local;
      driver = wd.promiseChainRemote(serverConfig);
      require("./helpers/logging").configure(driver);
      var appDesired = _.clone(require("./helpers/caps").ios82);
	  appDesired.app = "/Users/utelly/Desktop/AppiumAppTest/build/Release-iphonesimulator/AppiumAppTest.app";
      return driver.init(appDesired);
  });
  after(function(){
   	// stops and turns off the browser
   	return driver
   	  .quit()
   	  .finally(function() {
   	  });
  });
  beforeEach(function(){
    // runs before each test in this block
  });
  afterEach(function(){
    // runs after each test in this block
    
  });
   
  function populate() {
    var seq = _(['Win my first game of ping-pong versus Vincent', 'Prepare the coffee', 'go to the grocery store']).map(function (name) {
      return driver.waitForElementByAccessibilityId('NewThingToDoTextField', 3000).then(function(elt) {
	    return elt.type(name).elementByName('return').click().then(function() {
	      return driver.elementByAccessibilityId('AddThingToDoButton').click().sleep(1000);
	    });  
	  });
    });
    return seq.reduce();
  } 
   
   
  // defines tests cases
  it('Login textfield', function(){
    return driver.waitForElementByAccessibilityId('Login', 3000).then(function(elt) {
  	  return elt.type('Alexandre').elementByName('return').click();
  	});
  });
  it('Password textfield', function(){
    return driver.waitForElementByAccessibilityId('Password', 3000).then(function(elt) {
  	  return elt.type('password').elementByName('return').click();
  	});
  });
  it('Password forgotten button', function(){
    return driver.elementByAccessibilityId('PasswordButton').click().sleep(1000).then(function() {
      return driver.elementByName('That\'s sure').click().sleep(3000);
    });           
  });
  it('Login button:', function(){
    return driver.elementByAccessibilityId('LoginButton').click().sleep(1000)
  }); 
  it('Add new things to do: ', function() {
    return populate().sleep(3000);
  });
});
