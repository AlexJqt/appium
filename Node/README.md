# prerequisites

Create your app build in the app directory:
```
xcodebuild -sdk iphonesimulator
```

Upgrade Mocha to the latest version:
```
npm install -g -f mocha
```

Install local packages:
```
npm install
```


# to run tests 
```
mocha [test.js]
```


# wd commands

* select a view's element :
	by name :               driver.elementByName('name');
	by accessibility id :   driver.elementByAccessibilityId('access_id');
    by tag :                driver.elementByTagName('tag'); 
    
* wait for something and select a view's element (usefull with textfield and button): 
	by _ : driver.waitForElementBy_('value', delay: int).then(function() { });

ex : 
	- Login is a UITextField :
	 	driver.waitForElementByAccessibilityId('Login', 3000).then(function(elt) { 
  	  	  return elt.type('Alexandre').elementByName('return').click();
  		});
  	- LoginButton is a UIButton : 
  		driver.elementByAccessibilityId('LoginButton').click().sleep(1000).then(function() {
          return driver.elementByName('Ok').click().sleep(3000);
        }); 
    
* check if one view's element exits (if we try to select an undefined element, the test crashes)  : 
	by _ : driver.hasElementBy_('value');

* select a view's element or continue the test if it is not found : 
	by _ : driver.elementByNameIfExits('value');
	
* click on an element : 
    element : element.click();
    
* get the visible text for an element :
	element : element.text(function(err, txt) {});
    
    