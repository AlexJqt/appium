"use strict";

require("./helpers/setup");

var wd = require("wd"), //With this module, we can select different elements of the view and perform some actions ...
    _ = require('underscore'),
    Q = require('q'),
    serverConfigs = require('./helpers/appium-servers');

describe("Simple Login Page", function () { //For mocha integration ie mocha ios-simple.js to run the script
  this.timeout(300000);
  var driver;
  var allPassed = true;

  before(function () { //Select the localhost server, the iOS app and the simulator
    var serverConfig = process.env.SAUCE ?
      serverConfigs.sauce : serverConfigs.local;
    driver = wd.promiseChainRemote(serverConfig); //Select the appium server as interface
    require("./helpers/logging").configure(driver); //Configure the script terminal

	process.env.DEV = true; //Boolean to specify we don't use the saucelab cloud
    var desired = _.clone(require("./helpers/caps").ios81); //Select the iOS simulator
    desired.app = require("./helpers/apps").iosTestApp; //Select the specific app
    if (process.env.SAUCE) {
      desired.name = 'ios - simple';
      desired.tags = ['sample'];
    }
    return driver.init(desired); //return the initialized driver
  });

  after(function () {
    return driver
      .quit()
      .finally(function () {
        if (process.env.SAUCE) {
          return driver.sauceJobStatus(allPassed);
        }
      });
  });

  afterEach(function () {
    allPassed = allPassed && this.currentTest.state === 'passed';
  });

  /*
  function populate() {
    var seq = _([{'key': 'Login', 'value': 'alexandre.jacquemot'}, {'key': 'Password', 'value': 'password'}]).map(function (textfield) {
      return function (connect) {
        return driver.waitForElementByAccessibilityId(textfield.key, 3000).then(function (el) {
          return el.type(textfield.value).then(function () { return connect; })
              .elementByName('return').click().sleep(1000); // dismissing keyboard
        }).then(function () { return connect; });
      };
    });
    return seq.reduce(Q.when, new Q(0));
  }
  */
  
  it("should login", function () {
  	return driver.sleep(3000).then(function(fillOut) {
  		return driver.waitForElementByAccessibilityId('Login', 3000).then(function(el) {
  		
  		});
  	
  	}).sleep(3000);
  /*
    return driver
      .resolve(populate()).then(function (connect) {
        return driver.
          elementByAccessibilityId('LoginButton')
            .click().sleep(1000)
      }).elementByName('Ok').click().sleep(3000);
      */
  });
});
