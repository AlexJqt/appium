__author__ = 'Alexandre Jacquemot'
__company__ = 'Utelly LTD'
__test_name___ = 'Test'

#Template for Appium UnitTest
# /!\ 

import unittest
import os
from appium import webdriver
from time import sleep
from appium.webdriver.common.touch_action import TouchAction
from desired_app import get_desired_capabilities

class TemplateTestCase(unittest.TestCase): # /!\ Change Template by your test name

    def setUp(self):
        """Set up Appium"""
        app = os.path.abspath('/Users/utelly/.jenkins/jobs/Utelly/workspace/app/Applications/Utelly.app') #Specify your app path

        self.driver = webdriver.Remote(
            command_executor = 'http://macmini.local:4723/wd/hub',
            desired_capabilities = get_desired_capabilities(app) #Specify the app path, the plateform version (default value: '8.2'), the device (default value: 'iPhone 5')
        )

    def test_exemple(self):
        """Test exemple"""
        button = self.driver.find_element_by_accessibility_id('MyButton') #Find MyButton on the view
        self.assertIsNotNone(button) #Control if MyButton exists, if it doesn't exist, the test stops
        get_started_button.click() #Click on the button
        sleep(3) #Introduce a delay
        
    def test_another_exemple(self):
    	"""Test another exemple"""
    	el1 = self.driver.find_element_by_name('CellTitle0')
        self.assertIsNotNone(el1)
        el2 = self.driver.find_element_by_name('CellTitle5')
        self.assertIsNotNone(el2)
        self.driver.scroll(el2, el1)
        sleep(1)

    def tearDown(self):
        """Quit Appium"""
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
