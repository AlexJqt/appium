import os

# Returns abs path relative to this file and not cwd
#PATH = lambda p: os.path.abspath(
#    os.path.join(os.path.dirname(__file__), p)
#)


def get_desired_capabilities(app, platform_version = '8.3', device_name = 'iPhone 6'):
    desired_caps = {
        'deviceName': 'iPhone Simulator',
        'platformName': 'iOS',
        'platformVersion' : platform_version,
        'deviceName' : device_name,
        'app': app,
    }

    return desired_caps