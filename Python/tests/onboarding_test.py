__author__ = 'Alexandre Jacquemot'
__company__ = 'Utelly LTD'
__test_name__ = 'Onboarding Test'


 # # # # # # # # # # # # # # # # # # # # # # # # DESCRIPTION # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                                                                                                   #
#  I. Get started use case :                                                                                        #
#  1. Test if at least, 10 regions are showing and if there are "London", "Nothern Island", "Scottish" in the list  #
#  2. Test if there are 5 packages and if they are equal to [M, More, M+, L, XL]                                    #
#  3. Test if we can click on each package                                                                          #
#  4. Test if all services are selected for each package                                                            #
#  5. Test the "why?" text                                                                                          #
#  6. Test if you can't click on "enjoy TV" if we haven't select a gender                                           #
#  7. Test if we can select any gender                                                                              #
#                                                                                                                   #
#  II. Signin use case :                                                                                            #
#  1. Test if there are 6 buttons on the view                                                                       #
#  2. Test Facebook signin                                                                                          #
#  3. Test Twitter sigin                                                                                            #
#  4. Test Google signin                                                                                            #
#  5. Test Email signin                                                                                             #
#                                                                                                                   #
 # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

import unittest
import os
from appium import webdriver
from time import sleep
from appium.webdriver.common.touch_action import TouchAction
from capabilities.desired_app import get_desired_capabilities

class OnboardingTestCase(unittest.TestCase):

    def setUp(self):
        """Set up Appium"""
        app = os.path.abspath('/Users/macmini/.jenkins/workspace/Utelly/DerivedData/Build/Products/Debug-iphonesimulator/Utelly Dev.app')

        self.driver = webdriver.Remote(
            command_executor = 'http://127.0.0.1:4723/wd/hub',
            desired_capabilities = get_desired_capabilities(app)
        )
        
    def test_get_started(self):
        """Test the get started use case"""

        ##################################################################### VIEW 1 #####################################################################

        #Click on the get started button
        get_started_button = self.driver.find_element_by_accessibility_id('GetStarted')
        self.assertIsNotNone(get_started_button)
        get_started_button.click()
        sleep(1)

        ##################################################################### VIEW 2 #####################################################################

        #Test if at least, 10 regions are showing and if there are "London", "Nothern Island", "Scottish" in the list
        cells = self.driver.find_elements_by_class_name('UIATableCell')
        self.assertGreaterEqual(len(cells), 10)
        self.assertIn("London", [x.get_attribute('name') for x in cells])
        self.assertIn("Northern Ireland", [x.get_attribute('name') for x in cells])
        self.assertIn("Scottish", [x.get_attribute('name') for x in cells])

        #Scroll from element 1 to element 2
        el1 = self.driver.find_element_by_name('London')
        self.assertIsNotNone(el1)
        el2 = self.driver.find_element_by_name('Granada')
        self.assertIsNotNone(el2)
        self.driver.scroll(el2, el1)
        sleep(0.5)
        
        #Tap on the top of the screen to scroll to the top 
        self.driver.tap([[160, 5]], 1)
        
        #Select London
        el1.click()
        sleep(0.5)
        
        self.driver.find_element_by_name('Next').click()
        sleep(1)

        ##################################################################### VIEW 3 #####################################################################

        #Test if there are 5 packages and if they are equal to [M, More, M+, L, XL]
        segmented_control = self.driver.find_element_by_class_name('UIASegmentedControl').find_elements_by_class_name('UIAButton')
        self.assertIsNotNone(segmented_control)
        self.assertEqual(5, len(segmented_control))
        self.assertEqual([x.get_attribute('name') for x in segmented_control], ['M', 'More', 'M+', 'L', 'XL'])

        #Test if all services are selected for each package and if we can click on each package
        #service_buttons =  self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]').find_elements_by_class_name('UIAButton')
        #self.assertEqual(11, len(service_buttons)) It doesn't work for the moment ...
        for state in segmented_control:
            state.click()
            #self.assertEqual(state.get_attribute('value') , '1') #This functionality doesn't look to be implemented ...
            sleep(0.5)
            #for service_button in service_buttons:
                #self.assertEqual(service_button.get_attribute('value') , 1)

        #Test the "why?" text
        why_button = self.driver.find_element_by_name('why?')
        self.assertIsNotNone(why_button)
        why_button.click()
        sleep(0.5)

        why_text = self.driver.find_element_by_name('This will help us only include programs you can actually watch.')
        self.assertIsNotNone(why_button)
        sleep(0.5)

        self.driver.find_element_by_name('Next').click()
        sleep(1)

        ##################################################################### VIEW 4 #####################################################################

        #Test if you can't click on "enjoy TV" if we haven't select a gender
        enjoy_tv_button = self.driver.find_element_by_name('Enjoy TV')
        self.assertIsNotNone(enjoy_tv_button)
        #self.assertFalse(enjoy_tv_button.get_attribute('enabled')) #This functionality isn't implemented in Appium, possible with selenium ...

        #Test if we can select any gender
        male_button = self.driver.find_element_by_name('MaleButton')
        self.assertIsNotNone(male_button)
        female_button = self.driver.find_element_by_name('FemaleButton')
        self.assertIsNotNone(female_button)
        male_button.click()
        sleep(0.5)
        female_button.click()
        sleep(0.5)

        age_text_field = self.driver.find_element_by_class_name('UIATextField')
        self.assertIsNotNone(age_text_field)
        age_text_field.click()
        sleep(0.5)
        
        done_button = self.driver.find_element_by_name('Done')
        self.assertIsNotNone(done_button)
        done_button.click()
        sleep(3)

        ##################################################################### VIEW 5 #####################################################################

    def test_signin(self):
        """Test the signin use case"""

        ##################################################################### VIEW 1 #####################################################################

        #Click on the signin button
        signin_button = self.driver.find_element_by_accessibility_id('SignIn')
        self.assertIsNotNone(signin_button)
        signin_button.click()
        sleep(1)

        ##################################################################### VIEW 2 #####################################################################

        #Test if there are 6 buttons on the view
        scroll_view = self.driver.find_element_by_class_name('UIAScrollView')
        self.assertIsNotNone(scroll_view)

        social_buttons = scroll_view.find_elements_by_class_name('UIAButton')
        self.assertEqual(len(social_buttons), 6)

        #Test Email signin
        self.populate_email_login('alexandre@utelly.com', 'password')

        email_button = self.driver.find_element_by_name('btn login email')
        self.assertIsNotNone(email_button)
        email_button.click()

        self.populate_email_login('appium@utelly.com', 'utelly')
        sleep(5)

    def tearDown(self):
        """Quit Appium and send the Slack report"""
        self.driver.quit()

    def populate_email_login(self, login, password):
        """Populate the login textfields"""
        email_button = self.driver.find_element_by_name('btn login email')
        self.assertIsNotNone(email_button)
        email_button.click()

        email_login_text_field = self.driver.find_element_by_class_name('UIATextField')
        email_login_text_field.click()
        sleep(1)
        email_login_text_field.set_value(login)
        self.assertEqual(email_login_text_field.get_attribute('value'), login)
        sleep(0.5)

        next_button_keyboard = self.driver.find_element_by_name('Next')
        self.assertIsNotNone(next_button_keyboard)
        next_button_keyboard.click()
        sleep(0.5)

        email_password_text_field = self.driver.find_element_by_class_name('UIASecureTextField')
        email_password_text_field.click()
        sleep(0.5)
        email_password_text_field.set_value(password)
        sleep(0.5)

        done_button_keyboard = self.driver.find_element_by_name('Done')
        self.assertIsNotNone(done_button_keyboard)
        done_button_keyboard.click()
        sleep(0.5)

        login_button = self.driver.find_element_by_name('btn email login')
        self.assertIsNotNone(login_button)
        login_button.click()
        sleep(1)


if __name__ == '__main__':
    unittest.main()
