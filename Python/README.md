Appium Unit Tests in Python
===========================

Appium Client (anyone):
-----------------------

### To create a Python Appium Test ###

Download the [Appium client](https://github.com/appium/appium/releases) Application 

Configure the Appium Client to know the name/id/class of each view element
`````````````````````````````````````````
1. Click on the Apple icon
2. Specify the .app path 
3. Click on the Launch button to start the Appium Local Server
4. Click on the Search button to launch the Appium Inspector and the iOS Simulator
5. Get the name/id/class of each view element

`````````````````````````````````````````

Install the Appium client library and pytest:
`````````````````````````````````````````````````````````````
> pip install Appium-Python-Client	#get Appium Python Client`
> pip install pytest				#get Pytest
`````````````````````````````````````````````````````````````

Create an Appium test from the test template and put it in the tests directory:
````````````````````````````````````````
> cp template_test.py [your_unittest.py]
> mv [your_unittest.py] tests
> open -a [your_text_editor] app_test.py
````````````````````````````````````````

### To test your test in local

Download the app to generate the .app from the ios-utelly repository

Generate the .app:
`````````````````````````````````
pod install

xcodebuild -sdk iphonesimulator \
  -workspace Utelly.xcworkspace \
  -scheme Utelly \
  ONLY_ACTIVE_ARCH=NO \
  TARGETED_DEVICE_FAMILY=1 \
  DEPLOYMENT_LOCATION=YES \
  DSTROOT=`pwd`/app
`````````````````````````````````

Specify the local path to the .app in the python test (get_desired_capabilities(app = app_path))

Run the test on the Local Appium Client Server:
```````````````````````````````````````````````````````````````````
First terminal :
> appium

Second terminal :
> py.test [your_unittest.py]	#run your test on the Appium server
```````````````````````````````````````````````````````````````````

### When the test is ready, push your work on the Appium repository ###

Appium Server (mac mini):
-------------------------

Install Homebrew, Node and Appium (no sudo !)
````````````````````````````````````````````````````````````````````````````````````````````````````````
> ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"	#get brew
> brew install node      																	#get Node.js
> npm install -g appium   																	#get Appium
> appium                 																	#run Appium
````````````````````````````````````````````````````````````````````````````````````````````````````````

Install the Appium client library and pytest:
`````````````````````````````````````````````````````````````
> pip install Appium-Python-Client	#get Appium Python Client
> pip install pytest				#get Pytest
`````````````````````````````````````````````````````````````

Run the test on the local Server:
```````````````````````````````````````````````````````````
> py.test ios_simple.py	#run your test on the Appium server
```````````````````````````````````````````````````````````


